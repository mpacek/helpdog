#!/bin/bash
date=`date +%d-%m_%H-%M`;
dir="$USER-$date";
logsTable=("/var/log/Xorg.0.log" "/var/log/auth.log" "/var/log/deamon.log" "/var/log/kern.log" "/var/log/syslog" "/var/log/cups/error_log");
logfile="log.log";
logPath="/tmp/$dir/";
logFilePath="$logPath/$logfile";
hashmargin="###";
mkdir $logPath
touch "$logPath/$logfile";


INPUT=/tmp/menu.sh.$$
 
OUTPUT=/tmp/output.sh.$$

dialog --backtitle 'Linux Mint Polska' --title 'Ahoj!' --msgbox 'Witaj w programie, który pomoże Ci przekazać \
polskiej społeczności Linux Mint dane dot. Twojego systemu. Pozwoli to na diagnozę oraz szybsze rozwiązanie problemu.' 8 80
dialog --inputbox "Podaj swój nick z Forum Liunx Mint (www.forum.linuxmint.pl):" 8 40 2>/tmp/input.$$
nick=`cat /tmp/input.$$`;
dialog --title 'Nick' --msgbox "Witaj $nick!" 8 30


# pobranie danych z logow
function getData()
{
        echo $hashmargin >> $2;
        echo "# BEGIN OF $1" >> $2;
        cat "$1" >> $2;
        echo "# END OF $1" >> $2;
        echo $hashmargin >> $2;
}

# petla zbierajaca dane z logow z logsTable[]
function loopTable()
{
for i in "${logsTable[@]}"
do
getData "$i" "$logFilePath"
done
}
function showDone()
{
dialog --title "[ W Y K O N A N O ]" --msgbox "Gotowe! \n Plik zaposano w $logFilePath" 8 100;
}

while true
do 
dialog --clear  --help-button --backtitle "Linux Mint Polska" \
--title "[ G Ł Ó W N E - M E N U ]" \
--menu "Użyj strzałek GÓRA/DÓŁ aby poruszać się po menu \n\
Wybierz który log chcesz spakować" 15 50 4 \
Xorg "Xorg.0.log" \
Auth "auth.log" \
Deamon "deamon.log" \
Kern "kern.log" \
Syslog "syslog" \
Cups "cups/error_log" \
All "Wszystkie powyżej" \
Exit "Wyjdź z programu" 2>"${INPUT}"
 
menuitem=$(<"${INPUT}")
 
 
# make decsion 
case $menuitem in
	Xorg) getData "${logsTable[0]}" "$logFilePath"; showDone;;
	Auth) getData "${logsTable[1]}" "$logFilePath"; showDone;;
	Deamon) getData "${logsTable[2]}" "$logFilePath"; showDone;;
	Kern) getData "${logsTable[3]}" "$logFilePath"; showDone;;
  Syslog) getData "${logsTable[4]}" "$logFilePath"; showDone;;
  Cups)  getData "${logsTable[5]}" "$logFilePath"; showDone;;
  All) loopTable; showDone;;
  Exit) echo "PA!" && clear; break;;
esac
done

echo "FORUM USER: $nick" >> $logFilePath;

